import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';

import { environment } from '../environments/environment';
import  { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { ProductsComponent } from './components/Products/Products.component';

import { ProductService } from './services/Product.service';
import { CitiesService } from './services/cities.service';
import { AddProductComponent } from './components/add-Product/add-Product.component';

import { FormsModule } from '@angular/forms';

import {AccordionModule} from 'primeng/accordion';
import {AutoCompleteModule} from 'primeng/autocomplete';
import {MenuItem} from 'primeng/api';
import {CheckboxModule} from 'primeng/checkbox';
import {DataTableModule} from 'primeng/datatable';
import {CarouselModule} from 'primeng/carousel';
import {DataListModule} from 'primeng/datalist';
import {DataViewModule} from 'primeng/dataview';
import {TabViewModule} from 'primeng/tabview';
import {FileUploadModule} from 'primeng/fileupload';
import {PickListModule} from 'primeng/picklist';
import {OrderListModule} from 'primeng/orderlist';
import {MenubarModule} from 'primeng/menubar';
import {DialogModule} from 'primeng/dialog';
import {CalendarModule} from 'primeng/calendar';
import {TabMenuModule} from 'primeng/tabmenu';
import {SliderModule} from 'primeng/slider';

import {ListaComponent} from './components/lista/lista.component';
import {TablaComponent} from './components/tabla/tabla.component';
import {HomeComponent} from './components/home/home.component';
import {DistanceComponent} from './components/distance/distance.component';

import { HttpModule } from '@angular/http';

import { ChatComponent } from './components/chat/chat.component';
import { AuthService } from './services/auth.service';
import { DbService } from './services/db.service';


import { AngularFireAuthModule } from 'angularfire2/auth';
import {HttpClientModule, HttpClient} from '@angular/common/http';
import { AngularFireDatabaseModule } from 'angularfire2/database-deprecated'

import { RouterModule, Routes } from '@angular/router';
import { CitiesComponent } from './components/cities/cities.component';


const appRoutes: Routes = [
  { path: 'products', component: ProductsComponent },
  { path: 'addProduct', component: AddProductComponent },
   { path: 'lista', component: ListaComponent },
  { path: 'tabla', component: TablaComponent },
  { path: 'chat', component: ChatComponent },
  { path: 'home', component: HomeComponent }
];


@NgModule({
  declarations: [
    AppComponent,
    ProductsComponent,
    AddProductComponent,
    ListaComponent,
    TablaComponent,
    HomeComponent,
    ChatComponent,
    DistanceComponent,
    CitiesComponent
 
  ],
  imports: [
    BrowserModule,
    AngularFireModule.initializeApp(environment.firebase, 'angularfs'),
    AngularFirestoreModule,
    AutoCompleteModule,
    FormsModule,
    CheckboxModule,
    DataTableModule,
    CarouselModule,
    DataListModule,
    DataViewModule,
    TabViewModule,
    FileUploadModule,
    PickListModule,
    OrderListModule,
    MenubarModule,
    DialogModule,
    BrowserAnimationsModule,
    CalendarModule,
    HttpClientModule,
    HttpModule,
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    TabMenuModule,
    SliderModule
  ],
  providers: [
    ProductService,AuthService,DbService,CitiesService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
