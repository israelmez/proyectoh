export interface Product {
    id?: string;
    name?: string;
    place?: string;
    cname?: string;
    cphone?: string;
    need?: string;
    date?: string;
  }

  