import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';

import { ProductService } from '../../services/Product.service';
import { Product } from '../../models/Product';
import { CitiesComponent } from '../cities/cities.component';

@Component({
  selector: 'app-add-Product',
  templateUrl: './add-Product.component.html',
  styleUrls: ['./add-Product.component.css'],
  encapsulation: ViewEncapsulation.None
})

export class AddProductComponent implements OnInit {

  @ViewChild(CitiesComponent)
  private city: CitiesComponent;

  Product: Product = {
   name: '',
   place: '',
   cname: '',
   cphone: '',
   need: '',
   date: '',
  };
 
  constructor(private ProductService: ProductService) {
  }

  ngOnInit() {
  }

  onSubmit() {
    if(this.Product.name != '' 
    && this.city.city != '' 
    && this.Product.cname != ''
    && this.Product.cphone != '' 
    && this.Product.need != ''
    && this.Product.date != '')
    
    {
      this.Product.place = this.city.city.name;
      this.ProductService.addProduct(this.Product);
      this.Product.name = '';
      this.Product.place = '';
      this.Product.cname = '';
      this.Product.cphone = '';
      this.Product.need = '';
      this.Product.date = '';
     
    }

  }

  today: number = Date.now();

}
