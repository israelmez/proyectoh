import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-distance',
  templateUrl: './distance.component.html',
  styleUrls: ['./distance.component.css']
})
export class DistanceComponent implements OnInit {

  @Output() onDistanceChange = new EventEmitter<number>();
  @Output() stopFilter = new EventEmitter<void>();

  filter: boolean = false;
  disableFilter: boolean = true;
  disableSlider = true;
  distance: number = 100;
  latitude?: number;
  longitude?: number;

  constructor() { }

  ngOnInit() {
    if (window.navigator.geolocation) {
      window.navigator.geolocation.getCurrentPosition((position) => { 
        var latitude = position.coords.latitude; 
        var longitude = position.coords.longitude;
        this.disableFilter= false;
       });
    }
  }

  onCheckboxChange(e) {
    this.disableSlider= !e;
    this.filter = e;
    if (e == true)
      this.onDistanceChange.emit(this.distance);
    else
      this.stopFilter.emit();
  }

  handleSliderChange(e) {
    this.onDistanceChange.emit(this.distance);
  }

}
