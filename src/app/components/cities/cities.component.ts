import { Component, OnInit } from '@angular/core';

import { CitiesService } from '../../services/cities.service';

const H = window['H'];

@Component({
  selector: 'app-cities',
  templateUrl: './cities.component.html',
  styleUrls: ['./cities.component.css']
})
export class CitiesComponent implements OnInit {

  city: any;

  filteredCities: any[];

  cities: any[];

  platform = new H.service.Platform({
    'app_id': 'TvLRh302Uii5r4PkDhqL',
    'app_code': '-1IQ506Zwd4DnuzLwLWYTw',
    'useHTTPS': true
  });

  constructor(public CitiesService: CitiesService) {}

  ngOnInit() {

    this.cities = this.CitiesService.getCities();

    if (window.navigator.geolocation) {
      window.navigator.geolocation.getCurrentPosition((position) => { 
        var latitude = position.coords.latitude; 
        var longitude = position.coords.longitude;
        this.getLocalCity(latitude, longitude);
       });
    };
  }

  getLocalCity(latitude: number, longitude: number) {

    var reverseGeocodingParameters = {
      prox: latitude + ',' + longitude + ',0',
      mode: 'retrieveAddresses',
      maxresults: 1,
      gen: 9,
      language: 'eng',
      locationattributes: 'none,ar',
      responseattributes: 'none',
      addressattributes: 'none,city,county'
    };

    function onSuccess(result) {
      var location = result.Response.View[0].Result[0];
      var curCity = location.Location.Address.City;
      var curCounty = location.Location.Address.County;

      var localCity = this.filterCityName(curCity);
      if (localCity == undefined) {
        localCity = this.filterCityName(curCounty);
      }

      if (localCity != '')
        this.city = localCity;
    };

    var geocoder = this.platform.getGeocodingService();

    geocoder.reverseGeocode(
      reverseGeocodingParameters,
      onSuccess.bind(this),
      function(e) {});
    
  }

  filterCities(event) {
    let query = event.query;
    this.filteredCities = this.filterCity(query, this.cities);
  }

  filterCity(query, cities: any[]):any[] {
    let filtered : any[] = [];
    for(let i = 0; i < cities.length; i++) {
        let city = cities[i];
        if(city.name.toLowerCase().indexOf(query.toLowerCase()) == 0) {
            filtered.push(city);
        }
    }
    return filtered;
  }

  filterCityName(byName : string) : any {
    for(let i = 0; i < this.cities.length; i++) {
      let foundCity = this.cities[i];
      if(foundCity.name.toLowerCase().indexOf(byName.toLowerCase()) == 0) {
          return foundCity;
      }
    }
  }

}
