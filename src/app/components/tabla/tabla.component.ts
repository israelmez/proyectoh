import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';

import { ProductService } from '../../services/Product.service';
import { CitiesService } from '../../services/cities.service';

import { Product } from '../../models/Product';

import { DistanceComponent } from '../distance/distance.component';
import {DataTableModule} from 'primeng/datatable';

const H = window['H'];

@Component({
  selector: 'app-tabla',
  templateUrl: './tabla.component.html',
  styleUrls: ['./tabla.component.css']
})
export class TablaComponent implements OnInit {

  @ViewChild(DistanceComponent) distance: DistanceComponent;
  @ViewChild(DataTableModule) dataTable: DataTableModule;

  Products: Product[];
  allProducts: Product[];
  editState: boolean = false;
  ProductToEdit: Product;
  encapsulation: ViewEncapsulation.None
  display: boolean = false;
  display2: boolean = false;
  latitude: number;
  longitude: number;
  localCity: string = '';
  filteredCities: string[];
  citiesDistance: Map<string, number> = new Map();

  platform = new H.service.Platform({
    'app_id': 'TvLRh302Uii5r4PkDhqL',
    'app_code': '-1IQ506Zwd4DnuzLwLWYTw',
    'useHTTPS': true
  });
  
  constructor(public ProductService: ProductService, public CitiesService: CitiesService) { }

  ngOnInit() {
    this.ProductService.getProducts().subscribe(Products => {
      //console.log(Products);
      this.Products = Products;
      this.allProducts = Products;
    });

    if (window.navigator.geolocation) {
      window.navigator.geolocation.getCurrentPosition((position) => {
        this.latitude = position.coords.latitude; 
        this.longitude = position.coords.longitude;
        this.fillCitiesMap();
       });
    };

  }

  fillCitiesMap() {
    var geocoder = this.platform.getGeocodingService();

    function onSuccess(result) {

      var location = result.Response.View[0].Result[0];
      var curCity = location.Location.Address.City;
      var distance = location.Distance;
     
      this.citiesDistance.set(curCity, distance);

    };

    for (var city of this.CitiesService.getCities()) {
      var geocodingParameters = this.getParameters(city.name);
      geocoder.geocode(
        geocodingParameters,
        onSuccess.bind(this),
        function(e) {});
    }
  }

  getParameters(cityName: string) {
    var geocodingParameters = {
      prox: this.latitude + ',' + this.longitude + ',0',
      maxresults: 1,
      gen: 9,
      city: cityName,
      language: 'eng',
      locationattributes: 'none,ar',
      responseattributes: 'none',
      addressattributes: 'none,city,county'
    };
    return geocodingParameters;
  }

  filterProductsByCities(cities: string[]) {
    var productsInCities: Product[] = [];
    for (var product of this.allProducts) {
      if (cities.includes(product["place"]))
        productsInCities.push(product);
    }
    this.Products = productsInCities;
  }

  stopFilterByCities() {
    this.Products = this.allProducts;
  }

  showDialog() {
    this.display = true;
  }

  showDialog2() {
    this.display2 = true;
  }

  filterByDistance(e) {

    var distanceInMeters = e * 1000;
    var geocoder = this.platform.getGeocodingService();

    this.filteredCities = [];

    this.getLocalCity(this.latitude, this.longitude);
    if (this.localCity != '')
    this.filteredCities.push(this.localCity);

    for (var city of this.CitiesService.getCities()) {
      if (this.citiesDistance.get(city.name) <= distanceInMeters)
        this.filteredCities.push(city.name);
    }

    this.filterProductsByCities(this.filteredCities);
    
  }

  getLocalCity(latitude: number, longitude: number) {

    var reverseGeocodingParameters = {
      prox: latitude + ',' + longitude + ',0',
      mode: 'retrieveAddresses',
      maxresults: 1,
      gen: 9,
      language: 'eng',
      locationattributes: 'none,ar',
      responseattributes: 'none',
      addressattributes: 'none,city,county'
    };

    function onSuccess(result) {
      var location = result.Response.View[0].Result[0];
      var curCity = location.Location.Address.City;
      var curCounty = location.Location.Address.County;

      var localCity = this.filterCityName(curCity);
      if (localCity == undefined) {
        localCity = this.filterCityName(curCounty);
      }

      if (localCity != '')
        this.localCity = localCity.name;
    };

    var geocoder = this.platform.getGeocodingService();

    geocoder.reverseGeocode(
      reverseGeocodingParameters,
      onSuccess.bind(this),
      function(e) {});
    
  }

  filterCityName(byName : string) : any {
    for(let i = 0; i < this.CitiesService.getCities().length; i++) {
      let foundCity = this.CitiesService.getCities()[i];
      if(foundCity.name.toLowerCase().indexOf(byName.toLowerCase()) == 0) {
          return foundCity;
      }
    }
  }

  stopFilterByDistance() {
    this.stopFilterByCities();
  }
}
